# tes3mp-pinger

Header-only library to ping tes3mp server.

Original ping code was written by Koncord.

### Compile CrabNet

1. Clone https://github.com/TES3MP/CrabNet.git
2. Follow instruction from [here](https://github.com/TES3MP/CrabNet#linux)

### How to use

- Clone this project into your project directory or whatever dir you want
- Add to your cmake project:

```cmake
include("tes3mp-pinger/tes3mp-pinger.cmake")
include_directories(tes3mp-pinger/)
```
