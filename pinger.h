#ifndef PINGER_H
#define PINGER_H

#define PING_UNREACHABLE 999

#include <RakPeer.h>
#include <MessageIdentifiers.h>
#include <RakSleep.h>
#include <GetTime.h>

#include <future>
#include <string>

class Pinger
{
    public:
        Pinger(const std::string &address, const int &port) :
            m_address(address),
            m_port(port)
        {}

        ~Pinger(){}

        bool status()
        {
            return m_status;
        }

        bool start_ping()
        {
            auto status = std::async(std::launch::async, &Pinger::ping, this);
            status.wait();

            if (status.get() == PING_UNREACHABLE)
                m_status = false;
            else
                m_status = true;

            return m_status;
        }

    private:
        int ping()
        {
            RakNet::Packet *packet;
            bool done = false;
            RakNet::TimeMS time = PING_UNREACHABLE;

            RakNet::SocketDescriptor socketDescriptor{0, ""};
            RakNet::RakPeerInterface *peer = RakNet::RakPeerInterface::GetInstance();
            peer->Startup(1, &socketDescriptor, 1);

            if (!peer->Ping(m_address.c_str(), m_port, false))
                return time;

            RakNet::TimeMS start = RakNet::GetTimeMS();

            while (!done)
            {
                RakNet::TimeMS now = RakNet::GetTimeMS();

                if (now - start >= PING_UNREACHABLE)
                    break;

                packet = peer->Receive();

                if (!packet)
                    continue;

                switch (packet->data[0])
                {
                    case ID_DISCONNECTION_NOTIFICATION:
                    case ID_CONNECTION_LOST:
                        done = true;
                        break;

                    case ID_CONNECTED_PING:
                    case ID_UNCONNECTED_PONG:
                    {
                        RakNet::BitStream bsIn(&packet->data[1], packet->length, false);
                        bsIn.Read(time);
                        time = now - time;
                        done = true;
                        break;
                    }

                    default:
                        break;
                }

                peer->DeallocatePacket(packet);
            }

            peer->Shutdown(0);
            RakNet::RakPeerInterface::DestroyInstance(peer);
            return time > PING_UNREACHABLE ? PING_UNREACHABLE : time;
        }

        std::string m_address;
        int m_port;
        bool m_status;
};

#endif // PINGER_H
