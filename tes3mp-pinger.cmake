set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(RAKNET_INCLUDE CACHE PATH "/usr/include/raknet")
set(RAKNET_LIB CACHE PATH "/usr/lib/libRakNetStatic.a")

find_package(Threads REQUIRED)

include_directories(${RAKNET_INCLUDE})

link_libraries(Threads::Threads)
link_libraries(${RAKNET_LIB})
